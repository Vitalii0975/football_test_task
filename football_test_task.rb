class LeagueTable
  def initialize
    @matches = []
  end

  def matches
    @matches
  end

  def get_points(team_name, points = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)

      teams, scores = match_data[:teams], match_data[:scores]
      home_team, away_team = teams
      home_score, away_score = scores


      points += home_score > away_score ? 3 : home_score == away_score ? 1 : 0 if home_team == team_name
      points += away_score > home_score ? 3 : away_score == home_score ? 1 : 0 if away_team == team_name
    end
    { team_name: team_name, points: points }
  end

  def get_goals_for(team_name, goals_for = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)

      teams, scores = match_data[:teams], match_data[:scores]

      home_team, away_team = teams
      home_score, away_score = scores

      goals_for += home_score if home_team == team_name
      goals_for += away_score if away_team == team_name
    end
    { team_name: team_name, scored_goals: goals_for }
  end

  def get_goals_against(team_name, goals_against = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)


      teams, scores = match_data[:teams], match_data[:scores]
      home_team, away_team = teams
      home_score, away_score = scores

      goals_against += away_score if home_team == team_name
      goals_against += home_score if away_team == team_name
    end
    { team_name: team_name, goals_against: goals_against }
  end

  def get_goal_difference(team_name)
    goals_for = get_goals_for(team_name)
    goals_against = get_goals_against(team_name)

    goals_for = goals_for[:scored_goals] if goals_for.is_a?(Hash)
    goals_against = goals_against[:goals_against] if goals_against.is_a?(Hash)

    goals_difference = goals_for - goals_against
    { team_name: team_name, goals_difference: goals_difference }
  end

  def get_wins(team_name, wins = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)

      teams, scores = match_data[:teams], match_data[:scores]

      home_team, away_team = teams
      home_score, away_score = scores

      wins += 1 if home_score > away_score && (home_team == team_name || away_team == team_name)
    end
    { team_name: team_name, wins: wins }
  end

  def get_draws(team_name, draws = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)

      teams, scores = match_data[:teams], match_data[:scores]

      home_team, away_team = teams
      home_score, away_score = scores

      draws += 1 if home_score == away_score && (home_team == team_name || away_team == team_name)
    end
    { team_name: team_name, draws: draws }
  end

  def get_losses(team_name, losses = 0)
    @matches.each do |match|
      match_data = extract_match_data(match)

      teams, scores = match_data[:teams], match_data[:scores]

      home_team, away_team = teams
      home_score, away_score = scores

      losses += 1 if home_score < away_score && (home_team == team_name || away_team == team_name)
    end
    { team_name: team_name, losses: losses }
  end

  private

  def extract_match_data(match)
    match_data = match.match(/(?<team1>.+) (?<score1>\d+) - (?<score2>\d+) (?<team2>.+)/)

    return puts 'Invalid data. Incorrect match format' unless match_data

    {
      teams: [match_data[:team1], match_data[:team2]],
      scores: [match_data[:score1], match_data[:score2]].map(&:to_i)
    }
  end

end

lt = LeagueTable.new

lt.matches.push("Man Utd 5 - 1 Liverpool")
lt.matches.push("Man Utd 2 - 0 Man City")
lt.matches.push("Man Utd 3 - 3 Tottenham")
lt.matches.push("Man Utd 2 - 3 Arsenal")

result_point = lt.get_points("Man Utd")
puts "Points #{result_point[:team_name]}: #{result_point[:points]} points"

scored_goals = lt.get_goals_for("Man Utd")
puts "Scored goals #{scored_goals[:team_name]}: #{scored_goals[:scored_goals]} scored goals"

goals_against = lt.get_goals_against("Man Utd")
puts "Missed goals #{goals_against[:team_name]}: #{goals_against[:goals_against]} goals against"

goals_difference = lt.get_goal_difference("Man Utd")
puts "Goal difference for #{goals_difference[:team_name]}: #{goals_difference[:goals_difference]}"

wins = lt.get_wins("Man Utd")
puts "Wins #{wins[:team_name]}: #{wins[:wins]}"

draws = lt.get_draws("Man Utd")
puts "Draws #{draws[:team_name]}: #{draws[:draws]}"

losses = lt.get_losses("Man Utd")
puts "Losses #{losses[:team_name]}: #{losses[:losses]}"

goals_difference = lt.get_goal_difference("Man Utd")
puts "Goal difference #{goals_difference[:team_name]}: #{goals_difference[:goals_difference]}"



