system 'rm Gemfile' if File.exist?('Gemfile')
File.write('Gemfile', <<-GEMFILE)
  source 'https://rubygems.org'
  gem 'rspec'
GEMFILE

require 'bundler'
Bundler.setup(:default)

system 'bundle install'

require 'rspec'

RSpec::Core::Runner.run(['./spec/football_test_task_spec.rb'])

require './spec/football_test_task_spec.rb'

require './football_test_task.rb'

