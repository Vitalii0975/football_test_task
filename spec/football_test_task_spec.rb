require_relative '../football_test_task'
# require 'rspec'

RSpec.describe LeagueTable do
  let(:league_table) { LeagueTable.new }

  describe "#get_points" do
    context 'when there are no matches' do
      it 'returns 0 points for any team' do
        expect(league_table.get_points('Man Utd')).to eq({ team_name: 'Man Utd', points: 0 })
      end
    end

    context 'when there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 1 - 2 Man Utd'])
      end

      it 'returns the correct points for each team' do
        expect(league_table.get_points('Man Utd')).to eq({ team_name: 'Man Utd', points: 3 })
        expect(league_table.get_points('Arsenal')).to eq({ team_name: 'Arsenal', points: 0 })
      end
    end
  end

  describe '#get_goals_for' do
    context 'when there are no matches' do
      it 'returns 0 goals for any team' do
        expect(league_table.get_goals_for('Man Utd')).to eq({ team_name: 'Man Utd', scored_goals: 0 })
      end
    end

    context 'when there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 3 - 2 Man Utd'])
      end

      it 'returns the correct number of goals for each team' do
        expect(league_table.get_goals_for('Man Utd')).to eq({ team_name: 'Man Utd', scored_goals: 2 })
        expect(league_table.get_goals_for('Arsenal')).to eq({ team_name: 'Arsenal', scored_goals: 3 })
      end
    end
  end

  describe "#get_goals_against" do
    context 'when there are no matches' do
      it 'returns 0 goals against for any team' do
        expect(league_table.get_goals_against('Man Utd')).to eq({ team_name: 'Man Utd', goals_against: 0 })
      end
    end

    context 'when there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 1 - 3 Man Utd'])
      end

      it 'returns the correct number of goals against for each team' do
        expect(league_table.get_goals_against('Man Utd')).to eq({ team_name: 'Man Utd', goals_against: 1 })
        expect(league_table.get_goals_against('Arsenal')).to eq({ team_name: 'Arsenal', goals_against: 3 })
      end
    end
  end

  describe "#get_goal_difference" do
    context 'when there are no matches' do
      it 'returns 0 goal difference for any team' do
        expect(league_table.get_goal_difference('Manchester United')[:goals_difference]).to eq(0)
      end
    end

    context 'when there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 0 - 1 Manchester United'])
      end

      it 'returns the correct goal difference for each team' do
        expect(league_table.get_goal_difference('Manchester United')[:goals_difference]).to eq(1)
        expect(league_table.get_goal_difference('Arsenal')[:goals_difference]).to eq(-1)
      end
    end
  end

  describe '#get_wins' do
    context 'when there are no matches' do
      it 'returns 0 wins for any team' do
        expect(league_table.get_wins('Man Utd')[:wins]).to eq(0)
      end
    end

    context 'when there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Man Utd 3 - 1 Arsenal'])
      end

      it 'returns the correct number of wins for each team' do
        expect(league_table.get_wins('Man Utd')[:wins]).to eq(1)
        expect(league_table.get_wins('Arsenal')[:wins]).to eq(1)
      end
    end
  end


  describe '#get_draws' do
    context 'when there are no matches' do
      it 'returns 0 draws for any team' do
        expect(league_table.get_draws('Man Utd')[:draws]).to eq(0)
      end
    end

    context 'where there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 1 - 1 Man Utd'])
      end

      it 'returns the correct number draws for each team' do
        expect(league_table.get_draws('Man Utd')[:draws]).to eq(1)
        expect(league_table.get_draws('Arsenal')[:draws]).to eq(1)
      end
    end
  end

  describe '#get_losses' do
    context 'when there are no matches' do
      it 'returns 0 losses for any team' do
        expect(league_table.get_losses('Man Utd')[:losses]).to eq(0)
      end
    end

    context 'where there are matches' do
      before do
        league_table.instance_variable_set(:@matches, ['Arsenal 1 - 1 Man Utd'])
      end

      it 'returns the correct number losses for each team' do
        expect(league_table.get_losses('Man Utd')[:losses]).to eq(0)
        expect(league_table.get_losses('Arsenal')[:losses]).to eq(0)
      end
    end
  end
end
